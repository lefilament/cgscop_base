.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


======================
CG SCOP - Base et Mail
======================

Modifie le module Base et Mail:

* rend invible le trombonne du chatter
* envoie notifications abonnés sur les champs en track_visibility
* hérite le bouton import pour permettre l'ajout d'un attribut import=false dans la treeview

Credits
=======

Funders
-------

The development of this module has been financially supported by:

    Confédération Générale des SCOP (https://www.les-scop.coop)


Contributors
------------

* Juliana Poudou <juliana@le-filament.com>
* Rémi Cazenave <remi@le-filament.com>
* Benjamin Rivier <benjamin@le-filament.com>


Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament

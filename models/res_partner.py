# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models


class ScopPartner(models.Model):
    _inherit = "res.partner"

    def _track_subtype(self, init_values):
        self.ensure_one()
        return 'cgscop_base.cg_values_change'

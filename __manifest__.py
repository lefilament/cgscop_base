{
    "name": "CG SCOP - Base et Mail",
    "summary": "CG SCOP - Base et Mail",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "maintainers": ["remi-filament"],
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "base",
        "mail",
    ],
    "data": [
        "views/assets.xml",
        "views/subtype.xml",
    ],
    'qweb': ['static/src/xml/templates.xml'],
}

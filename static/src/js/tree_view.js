odoo.define('cgscop_base.TreeviewImport', function(require) {
"use strict";

var AbstractView = require('web.AbstractView');

var TreeviewImport = AbstractView.include({
    init: function (viewInfo, params) {
        this._super(viewInfo, params);
        if (this.arch.attrs.import == "False") {
        	this.controllerParams.activeActions['import'] = false;
        } else {
        	this.controllerParams.activeActions['import'] = true;
        }
    },
    });

return TreeviewImport;

});